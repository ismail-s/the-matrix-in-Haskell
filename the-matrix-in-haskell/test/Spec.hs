import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Monadic (assert, monadicIO, run, pre)
import Control.Monad.Random (evalRandIO)
import Data.List (isInfixOf)
import Lens.Micro.Platform ((^.))
import Graphics.TheMatrixSimulation

main :: IO ()
main = hspec $ do
    describe "setTermWidthTo" $ do
        let exampleLine = Line 3 2 40 "some text"
        it "behaves like the id func when the term width doesn't change" $ do
            let f (Positive n) (Positive m) = n /= m || setTermWidthTo n st == st
                    where st = St (replicate m exampleLine)
            property f
        it "appends emptyLines when the term width is increased" $ do
            let f (Positive n) (Positive m) = n <= m || checkEmptyLines
                    where st = St (replicate m exampleLine)
                          res = (_linesToDisplay . setTermWidthTo n) st
                          checkEmptyLines = (all (==emptyLine) . drop m) res
            property f
        it "drops lines from the end when the term width is decreased" $ do
            let f (Positive n) (Positive m) = n >= m || expected == actual
                    where st = St (replicate (m - 1) exampleLine ++ [emptyLine])
                          expected = St (replicate n exampleLine)
                          actual = setTermWidthTo n st
            property f
        it "always sets the no. of lines in St to be the term width" $
            property $ \(Positive n) (Positive m) -> n ==
                (length . _linesToDisplay . setTermWidthTo n . St . replicate m) exampleLine

    describe "nCharsAround" $ do
        let checkThat f = property $ \n i s -> f (nCharsAround n i s) s
        it "can return the first char of a string" $
            property $ \s -> null s || (nCharsAround 1 0 s == ("", [head s], tail s))
        it "doesn't return more than n chars" $ do
            let f n i s = length b <= max n 0 where (_, b, _) = nCharsAround n i s
            property f
        it "returns a sublist of the string as its first tuple elem" $
            checkThat $ \(a, _, _) s -> a `isInfixOf` s
        it "returns a sublist of the string as its middle tuple elem" $
            checkThat $ \(_, b, _) s -> b `isInfixOf` s
        it "returns a sublist of the string as its last tuple elem" $
            checkThat $ \(_, _, c) s -> c `isInfixOf` s
        it "has the char at index i somewhere in the middle tuple elem" $ do
            let f n i s = (i < 0 || i >= length s || n < 1) || (s !! i) `elem` b
                    where (_, b, _) = nCharsAround n i s
            property f
        it "returns a tuple which is the original string, but split up" $
            checkThat $ \(a, b, c) s -> a ++ b ++ c == s

    describe "incrementState" $ do
        it "does nothing to an empty state" $
            incrementState (St []) `shouldBe` St []
        it "removes lines that have position >= 100" $
            incrementState (St [emptyLine, Line 1 1 100 "a", Line 1 1 101 "a", Line 1 1 99 "a"])
                `shouldBe` St [emptyLine, emptyLine, emptyLine, Line 1 1 100 "a"]
        it "increments lines by the correct amount" $
            property $ \n -> incrementState (St [Line 1 n 50 "a", Line 1 4 60 "a"])
                            == St [Line 1 n (50 + n) "a", Line 1 4 64 "a"]

    describe "addNewLineToState" $ do
        it "can add a new line to an empty state" $
            property $ \s -> monadicIO $ do
                St (line:[]) <- run $ evalRandIO $ addNewLineToState s (St [])
                assert $ line^.text == s
                assert $ line^.pos == 0
                assert $ 1 <= line^.fallingSpeed && line^.fallingSpeed <= 5
                assert $ line^.noOfCharsToDisplay >= min 2 (length s)
                assert $ line^.noOfCharsToDisplay <= min 6 (length s)
        it "appends the line when there is one existing line" $
            property $ \s -> monadicIO $ do
                let existingLine = Line 2 2 1 "a"
                St [a, b] <- run $ evalRandIO $ addNewLineToState s (St [existingLine])
                assert $ a == existingLine
                assert $ b^.text == s
        it "always adds the new line to the state" $
            property $ \s (Positive n) -> monadicIO $ do
                let existingLines = replicate n (Line 2 2 1 "a")
                St ls <- run $ evalRandIO $ addNewLineToState s (St existingLines)
                assert $ (1 == length (filter (\l -> l^.text == s) ls))
                        || s == "a"

    describe "genRandomString" $ do
        it "returns a string with only characters from the provided alphabet" $
            property $ \alpha l h -> monadicIO $ do
                res <- run $ evalRandIO $ genRandomString alpha l h
                assert $ all (`elem` alpha) res
        it "returns a string with a length in the supplied range" $
            property $ \alpha l h -> monadicIO $ do
                res <- run $ evalRandIO $ genRandomString alpha l h
                pre $ l <= h && not (null alpha)
                assert $ length res >= l || l < 0
                assert $ length res <= h || l < 0
