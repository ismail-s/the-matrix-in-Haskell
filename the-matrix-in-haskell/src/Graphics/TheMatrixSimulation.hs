{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Graphics.TheMatrixSimulation
    ( mainApp,
      incrementState,
      addNewLineToState,
      nCharsAround,
      St(..), linesToDisplay,
      Line(..), noOfCharsToDisplay, fallingSpeed, pos, text,
      emptyLine,
      setTermWidthTo,
      genRandomString
    ) where

import Lens.Micro.Platform ((^.), (&), (.~), (%~), makeLenses)
import Safe (atDef)
import Control.Monad (void, forever, replicateM)
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (newChan, writeChan, threadDelay, forkIO)
import Data.Default (def)
import Data.List (elemIndices)
import Data.Monoid ((<>))
import qualified System.Console.Terminal.Size as Term
import Control.Monad.Random (evalRandIO, MonadRandom, getRandomR, StdGen)
import Graphics.Vty
  ( Event, Attr, brightGreen, green, black, brightWhite
  )
import qualified Graphics.Vty as V

import Brick.Main
  ( App(..)
  , neverShowCursor
  , customMain
  , continue
  , halt
  )
import Brick.Types
  (availHeight, EventM(..), Size(..), getContext, Next(..), Widget(..))
import Brick.Widgets.Core
  ( hBox
  , vBox
  , str
  , withAttr
  )
import Brick.Util (on, fg)
import Brick.AttrMap (attrMap, AttrMap)
import Options.Applicative
  (argument, command, execParser, fullDesc, help, helper, hsubparser,
  idm, info, liftA2, metavar, Parser(..), progDesc)
import qualified Options.Applicative as Opt
import  System.IO (hGetLine, hWaitForInput, IOMode( ReadMode ) )
import System.IO.Error (catchIOError)
import System.Process (createProcess, proc, StdStream( CreatePipe ), std_out)

-- |Represents a column of falling text on-screen.
data Line = Line {
    _noOfCharsToDisplay :: Int,
    _fallingSpeed :: Int,
    _pos :: Int,
    _text :: String
    }
    deriving (Eq, Show)

emptyLine = Line 0 1 0 ""

-- |The state of the whole program. Basically just a list of 'Line' elements.
newtype St =
    St { _linesToDisplay :: [Line]}
    deriving (Eq, Show)

makeLenses ''Line
makeLenses ''St

data CustomEvent = VtyEvent Event
                 | Counter
                 | NewLine String

-- |Set the number of lines in @St l@ to @n@ by removing lines or adding
-- 'emptyLine's as necessary.
setTermWidthTo :: Int -> St -> St
setTermWidthTo n (St l)
    | length l == n = St l
    | length l < n = St $ l ++ replicate (n - length l) emptyLine
    | otherwise = St $ take n l

-- |Increment the positions of the falling strings in each column. If a
-- falling string has started going off the screen, then we replace it with
-- an 'emptyLine'.
incrementState :: St -> St
incrementState (St []) = St []
incrementState (St (l:ls)) = incrementState (St ls) & linesToDisplay %~ (newLine:)
    where newLine = if l^.pos >= 100 || l == emptyLine
                    then emptyLine
                    else l & pos %~ (+l^.fallingSpeed)

-- |Add the given string to the given state in a random position, with a random
-- fall speed, with a random amount of chars to display at a time on screen.
-- If possible, the given string will be put into a part of the state where
-- there was previously an 'emptyLine'. Otherwise, the string will be appended
-- to the state.
addNewLineToState :: (MonadRandom m) => String -> St -> m St
addNewLineToState s (St []) = liftA2 f n fallSpeed where
    f n fallSpeed = St $ pure $ Line n fallSpeed 0 s
    n = if length s > 1 then
        getRandomR (min 2 (length s), min 6 $ max 2 $ length s `div` 2)
        else return (length s)
    fallSpeed = getRandomR (1, 5)
addNewLineToState s (St [Line _ _ 0 ""]) = addNewLineToState s (St [])
addNewLineToState s (St [l]) = (linesToDisplay %~ (l:)) <$> addNewLineToState s (St [])
addNewLineToState s (St l) = case elemIndices emptyLine l of
    [] -> (linesToDisplay %~ (l++)) <$> addNewLineToState s (St [])
    indices -> do
        index <- getRandomR (0, length indices - 1)
        St l' <- addNewLineToState s (St [])
        return . St $ take index l ++ l' ++ drop (index + 1) l

-- |Return n characters around index i of the given string. A tuple is returned,
-- comprising (substring before n chars, n chars, remainder of string).
-- Depending on the index i, less than n chars may be returned, eg if the index
-- is out of bounds, or near the start/end of the string, or if @n > i@.
--
-- This function obeys the following law for all inputs:
--
-- prop> s == a ++ b ++ c where (a, b, c) = nCharsAround n i s
nCharsAround :: Int -> Int -> String -> (String, String, String)
nCharsAround n _ s | n <= 0 = (s, "", "")
nCharsAround _ _ "" = ("", "", "")
nCharsAround n i s = let
        startIndex = i - (n `div` 2)
        nCharsLen = if startIndex < 0 then n + startIndex else n
    in (take startIndex s,
        take nCharsLen (drop startIndex s),
        drop nCharsLen (drop startIndex s))

-- |Generate a random string using the supplied string as an alphabet. The
-- generated string has a length between the 2 numbers supplied. Note that the
-- first number must be smaller than the second number.
genRandomString :: (MonadRandom m) => String -> Int -> Int -> m String
genRandomString [] _ _ = return ""
genRandomString [c] low high = getRandomR (low, high) >>= (\n -> return (replicate n c))
genRandomString alphabet@(x:_) low high = do
    len <- getRandomR (low, high)
    let selectRandomCharFrom alpha = do
            index <- getRandomR (0, length alpha - 1)
            return $ atDef x alpha index
    replicateM len (selectRandomCharFrom alphabet)

fallingTextWidget :: Int -> Int -> String -> Widget n
fallingTextWidget n i s = Widget Fixed Fixed $ do
    ctx <- getContext
    let height = availHeight ctx
        pos = round $ (realToFrac i / 100) * realToFrac height
        (before, upToNChars, after) = nCharsAround n pos s
        (upToNMinus1Chars, lastChar) = splitAt (length upToNChars - 1) upToNChars
        mainStr = unlines $ replicate (length before) " " ++ map (: []) upToNMinus1Chars
        combinedStr = vBox [str mainStr, withAttr "white" $ str lastChar]
        strToShow = if s == "" then str " " else combinedStr
    render strToShow

drawUI :: St -> [Widget ()]
drawUI (St l) = [hBox $ map f l] where
    f line = fallingTextWidget (line^.noOfCharsToDisplay) (line^.pos) (line^.text)

appEvent :: St -> CustomEvent -> EventM () (Next St)
appEvent st e =
    case e of
        VtyEvent (V.EvKey V.KEsc []) -> halt st
        VtyEvent (V.EvResize w _) -> continue $ setTermWidthTo w st
        VtyEvent ev -> continue st
        Counter -> continue $ incrementState st
        NewLine s -> do
            newSt <- liftIO $ evalRandIO $ addNewLineToState s st
            continue newSt

initialState :: Int -> St
initialState n =
    St $ replicate n emptyLine

globalDefault :: Attr
globalDefault = brightGreen `on` black

ourAttrMap :: AttrMap
ourAttrMap = attrMap globalDefault
    [ ("white", fg brightWhite)
    ]

theApp :: App St CustomEvent ()
theApp =
    App { appDraw = drawUI
        , appChooseCursor = neverShowCursor
        , appHandleEvent = appEvent
        , appStartEvent = return
        , appAttrMap = const ourAttrMap
        , appLiftVtyEvent = VtyEvent
        }

data Command = Random | StdIn String deriving (Show)

opts :: Parser Command
opts = hsubparser (command "random" (info (pure Random)
                    (fullDesc <> progDesc "Show random text"))
                <> command "stdin" (info
                    (StdIn <$> argument Opt.str
                        (metavar "FILEPATH"
                        <> help "The file to tail"))
                    (fullDesc
                    <> progDesc "Tail a file, and show the \
                                \latest lines added to it")))

-- |Run the Matrix simulation using the command-line arguments passed to the
-- program.
mainApp :: IO ()
mainApp = do
    command <- execParser (info (helper <*> opts) idm)
    chan <- newChan

    case command of
        Random -> forkIO $ forever $ do
            let alphabet = "qwertyuiopasdfghjklzxcvbnm<>?:@{}!£$%^&*(_+)\",./;'[]1234567890-=#"
            line <- evalRandIO $ genRandomString alphabet 5 30
            writeChan chan (NewLine line)
            threadDelay 2000000
        StdIn filepath -> do
            (_, Just hout, _, _) <- createProcess (proc "tail" ["-f", filepath]){std_out = CreatePipe}
            forkIO $ forever $ do
                hWaitForInput hout (-1)
                line <- hGetLine hout
                writeChan chan (NewLine line)

    forkIO $ forever $ do
        writeChan chan Counter
        threadDelay 500000

    termSize <- Term.size
    let width = case termSize of
            Nothing -> 50
            Just w -> Term.width w
    void $ customMain (V.mkVty def) chan theApp $ initialState width
